package nl.belastingdienst.in2win.platformer;

import nl.belastingdienst.in2win.platformer.EntryPoint;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "In2Win";
		cfg.useGL20 = false;  // willen eigenlijk true hebben, maar camera.class vind dat niet leuk
		cfg.fullscreen = false;
		cfg.width = 1024;
		cfg.height = 768;
		
		new LwjglApplication(new EntryPoint(), cfg);
	}
}
