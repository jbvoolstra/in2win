package nl.belastingdienst.in2win.bijnalocaties;

import nl.belastingdienst.in2win.eventbus.Event;

public class BijnaBijToolshedLocatie extends BijnaBijLocatie {

	final String locatieNaam = "Bijna Bij ToolShed" ;
	
	public BijnaBijToolshedLocatie(BijnaBijLocatieType type) {
		super(type);
	}

	@Override
	public void perform(Event event) {
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
}
