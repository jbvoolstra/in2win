package nl.belastingdienst.in2win.bijnalocaties;

import nl.belastingdienst.in2win.eventbus.Event;

public class BijnaBijOnderwereldLocatie extends BijnaBijLocatie {

	public BijnaBijOnderwereldLocatie(BijnaBijLocatieType type) {
		super(type);
	}

	final String locatieNaam = "Bijna Bij Onderwereld" ;

	@Override
	public void perform(Event event) {
		
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
	
}
