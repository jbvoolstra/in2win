package nl.belastingdienst.in2win.bijnalocaties;

public enum BijnaBijLocatieType {
	BIJNABIJBELASTINGPARADIJS, BIJNABIJZWITSERSEBANK, BIJNABIJONDERWERELD, BIJNABIJBRAVEBURGER, BIJNABIJBELASTINGKANTOOR, BIJNABIJTOOLSHED;


	private BijnaBijLocatieType() {
	}
	
}
