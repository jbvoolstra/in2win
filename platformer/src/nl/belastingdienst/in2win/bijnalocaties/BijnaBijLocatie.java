package nl.belastingdienst.in2win.bijnalocaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventListener;

public abstract class BijnaBijLocatie implements EventListener {
	BijnaBijLocatieType type;
	Boolean		performActiesJN = false;
	
	public boolean ikBenVeroorzaker(Event event){
		
		if ( this.equals(event.getVeroorzaker()) ) { return true; }  
		return false;
		
	}
	
	public BijnaBijLocatie(BijnaBijLocatieType type) {
		this.type = type;
		
	}

	public BijnaBijLocatieType getType() {
		return type;
	}
	
	public abstract String getLocatieNaam();
	
}
