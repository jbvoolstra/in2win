package nl.belastingdienst.in2win.bijnalocaties;

import nl.belastingdienst.in2win.eventbus.Event;

public class BijnaBijZwitsersebankLocatie extends BijnaBijLocatie {

	final String locatieNaam = "Bijna Bij Zwitsersebank" ;

	public BijnaBijZwitsersebankLocatie(BijnaBijLocatieType type) {
		super(type);
	}

	/** 
	 *  Als deze Zwitsersebank is geregistreerd bij de EventBus, dan zal de EventBus 
	 *  bij elke BotstingEvent deze perform -methode aanroepen.
	 */
	@Override
	public void perform(Event event) {
	}


	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
	
}
