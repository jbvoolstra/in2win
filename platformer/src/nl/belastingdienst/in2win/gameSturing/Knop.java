package nl.belastingdienst.in2win.gameSturing;


public abstract class  Knop {
	KnopType type;
	
	
	public Knop(KnopType type) {
		this.type = type;
		
	}

	public KnopType getType() {
		return type;
	}
	
}
