package nl.belastingdienst.in2win.gameSturing;

public class PauzeKnop extends Knop {
	
	public PauzeKnop(KnopType type) {
		super(type);
	}

	private static Boolean pauzeAan = false;
	
	public static Boolean pauzeAanUit(){
		if (pauzeAan) {
			pauzeAan = false;
		}else {
			pauzeAan = true;
		}
		return pauzeAan;
	}

}
