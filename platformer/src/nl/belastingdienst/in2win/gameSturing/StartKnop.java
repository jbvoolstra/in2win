package nl.belastingdienst.in2win.gameSturing;

import nl.belastingdienst.in2win.actie.TijdverloopActie;
import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.platformer.GameController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

public class StartKnop extends Knop implements EventListener {
	private Body body;
	private EventBus eventBus;
	private boolean isSpelGestart = false;
	private Texture vageGroeneStarButton;
	private Texture GroeneStartButton;
	private SpriteBatch startBatch = new SpriteBatch();

	public StartKnop(KnopType type, GameController gameController,
			EventBus eventBus) {
		super(type);
		this.eventBus = eventBus;

		vageGroeneStarButton = new Texture(
				Gdx.files.internal("data/vageGroeneStartButton.png"));
		GroeneStartButton = new Texture(
				Gdx.files.internal("data/GroeneStartButton.png"));

		// registreer in de eventbus
		eventBus.register(EventType.TOETSINGEDRUKT, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.belastingdienst.in2win.eventbus.EventListener#perform(nl.belastingdienst
	 * .in2win.eventbus.Event)
	 */
	public void perform(Event event) {
		if (event.getTypeEvent() == EventType.TOETSINGEDRUKT) {
			if (event.getVeroorzaker() instanceof Knop) {
				Knop knop = (Knop) event.getVeroorzaker();
				if (knop.getType().equals(KnopType.STARTKNOP)) {
					if (!isSpelGestart) {
						isSpelGestart = true;
						eventBus.fireEvent(new Event(EventType.GESTART, this));
						TijdverloopActie.startKlok();
					}
				}
			}
		}
	}

	public boolean isSpelGestart() {
		return isSpelGestart;
	}

	public void setSpelGestart(boolean uitzetten) {
		isSpelGestart = uitzetten;
	}

	public static void update() {
		
		// Besturingsveld weet waar de startknop over getekend moet worden. ..
		// startBatch.begin();
		//
		// if (isSpelGestart) {
		// startBatch.draw(vageGroeneStarButton,
		// besturingsveld.getrondeStartButton().x,
		// besturingsveld.getrondeStartButton().y);
		// } else {
		// startBatch.draw(GroeneStartButton,
		// besturingsveld.getrondeStartButton().x,
		// besturingsveld.getrondeStartButton().y);
		//
		// }
		// startBatch.end();
	}
}