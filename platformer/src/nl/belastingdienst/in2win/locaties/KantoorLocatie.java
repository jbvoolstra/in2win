package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventType;

public class KantoorLocatie extends Locatie {

	private int afdracht;
	final String locatieNaam = "BelastingKantoor" ;
	private int opbrengst = 0;
	
	public KantoorLocatie(LocatieType type) {
		super(type);
		
		// Kantoorlocatie heeft geen gereedschap nodig
		setVoldoendeGereedschapAanwezig(true);
	}

	public int getAfdracht() {
		return afdracht;
	}

	public void setAfdracht(int afdracht) {
		this.afdracht = afdracht;
	}

	@Override
	public void perform(Event event) {

		if ( event.getTypeEvent() == EventType.CONTACT ) {
			if (ikBenVeroorzaker(event)) {
				
			}
		}
	}
	
	@Override
	public int getOpbrengst() {
		return this.opbrengst;
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
	
	@Override
	public void setOpbrengst(){
		this.opbrengst  = 0;
	}

	@Override
	public boolean isVoldoendeGereedschapAanwezig() {
		return voldoendeGereedschapAanwezig;
	}

	private void setVoldoendeGereedschapAanwezig(boolean voldoendeGereedschapAanwezig) {
			this.voldoendeGereedschapAanwezig = voldoendeGereedschapAanwezig;
	}
	
	@Override
	public void resetGereedschap() {
	}
	
}