package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventType;

public class HekLocatie extends Locatie {

	final String locatieNaam = "Hek" ;
	private int opbrengst = 0;
	
	public HekLocatie(LocatieType type) {
		super(type);
		
		// Heklocatie heeft geen gereedschap nodig
		setVoldoendeGereedschapAanwezig(true);
	}

	@Override
	public int getOpbrengst() {
		return this.opbrengst;
	}

	@Override
	public void perform(Event event) {
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
	
	@Override
	public void setOpbrengst(){
		this.opbrengst = 0;
	}

	@Override
	public boolean isVoldoendeGereedschapAanwezig() {
		return voldoendeGereedschapAanwezig;
	}

	private void setVoldoendeGereedschapAanwezig(boolean voldoendeGereedschapAanwezig) {
			this.voldoendeGereedschapAanwezig = voldoendeGereedschapAanwezig;
	}
	
	
	@Override
	public void resetGereedschap() {
	}
}
