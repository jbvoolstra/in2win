package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventType;

import com.badlogic.gdx.math.MathUtils;

public class BraveBurgerLocatie extends Locatie {
	
	private int opbrengst;
	final String locatieNaam = "Brave Burger" ;
	
	public BraveBurgerLocatie(LocatieType type ) {
		super(type);
		
		// BraveBurgerlocatie heeft geen gereedschap nodig
		setVoldoendeGereedschapAanwezig(true);
	}

	@Override
	public void perform(Event event) {	}
	
	@Override
	public int getOpbrengst(){
		return this.opbrengst;
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
	
	@Override
	public void setOpbrengst(){
		int geefGeheelGetal = MathUtils.random(1,3);
		this.opbrengst = LocatieType.BRAVEBURGER.getBasisOpbrengst() * geefGeheelGetal;
	}
	
	@Override
	public boolean isVoldoendeGereedschapAanwezig() {
		return voldoendeGereedschapAanwezig;
	}

	private void setVoldoendeGereedschapAanwezig(boolean voldoendeGereedschapAanwezig) {
			this.voldoendeGereedschapAanwezig = voldoendeGereedschapAanwezig;
	}
	
		@Override
	public void resetGereedschap() {
	}
}
