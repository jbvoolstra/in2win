package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gereedschap.GereedschapType;

import com.badlogic.gdx.math.MathUtils;

public class BelastingParadijsLocatie extends Locatie {

	private int opbrengst;
	final String locatieNaam = "BelastingParadijs" ;
	private int aantalBoten = 0;

	public BelastingParadijsLocatie(LocatieType type) {
		super(type);
	}

	@Override
	public void perform(Event event) {}
	
	@Override
	public int getOpbrengst(){
		return this.opbrengst;
	}
	
	@Override
	public void setOpbrengst(){
		int geefGeheelGetal = MathUtils.random(1,3);
		this.opbrengst = LocatieType.BELASTINGPARADIJS.getBasisOpbrengst() * geefGeheelGetal;
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}

	@Override
	public boolean isVoldoendeGereedschapAanwezig() {
		return voldoendeGereedschapAanwezig;
	}

	private void setVoldoendeGereedschapAanwezig(boolean voldoendeGereedschapAanwezig) {
			this.voldoendeGereedschapAanwezig = voldoendeGereedschapAanwezig;
	}

	@Override
	public void resetGereedschap() {
		this.aantalBoten = 0;
		this.setVoldoendeGereedschapAanwezig(false);
		System.out.println("gereedschap weer naar 0");
	}
	
	public void addBoot() {
		this.aantalBoten++;
		if (aantalBoten >= GereedschapType.BOOT.getAantal()  ) {
			this.setVoldoendeGereedschapAanwezig(true);
		}
	}
}
