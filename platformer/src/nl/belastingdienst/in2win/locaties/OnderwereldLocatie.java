package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.gereedschap.GereedschapType;

import com.badlogic.gdx.math.MathUtils;

public class OnderwereldLocatie extends Locatie {

	private int opbrengst;
	final String locatieNaam = "Onderwereld" ;
	private int aantalSchepjes = 0;
	
	public OnderwereldLocatie(LocatieType type) {
		super(type);
	}

	@Override
	public int getOpbrengst(){
		return this.opbrengst;
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}

	@Override
	public void setOpbrengst(){
		int geefGeheelGetal = MathUtils.random(1,3);
		this.opbrengst = LocatieType.ONDERWERELD.getBasisOpbrengst() * geefGeheelGetal;
	}

	@Override
	public void perform(Event event) {
		
	}

	@Override
	public boolean isVoldoendeGereedschapAanwezig() {
		return voldoendeGereedschapAanwezig;
	}

	public void setVoldoendeGereedschapAanwezig(boolean voldoendeGereedschapAanwezig) {
			this.voldoendeGereedschapAanwezig = voldoendeGereedschapAanwezig;
	}
	
	
	@Override
	public void resetGereedschap() {
		this.aantalSchepjes = 0;
		this.setVoldoendeGereedschapAanwezig(false);
		System.out.println("gereedschap weer naar 0");
	}
	
	public void addSchepje() {
		this.aantalSchepjes++;
		if (aantalSchepjes >= GereedschapType.SCHEP.getAantal()  ) {
			this.setVoldoendeGereedschapAanwezig(true);
		}
	}
	
}
