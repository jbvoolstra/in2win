package nl.belastingdienst.in2win.locaties;

public enum LocatieType {	BELASTINGPARADIJS(1500), 
							ZWITSERSEBANK(2000), 
							ONDERWERELD(4000), 
							BRAVEBURGER(200), 
							BELASTINGKANTOOR(), 
							TOOLSHED(),
							HEK();


//	Optioneel veld.
	private int basisOpbrengst;

	private LocatieType() {
	}

	private LocatieType(int basisOpbrengst) {
		this.basisOpbrengst = basisOpbrengst;
	}
	
	public int getBasisOpbrengst() {
			return basisOpbrengst;
	}
	
}