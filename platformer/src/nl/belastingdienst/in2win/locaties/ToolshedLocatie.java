package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventType;

public class ToolshedLocatie extends Locatie {

	final String locatieNaam = "ToolShed" ;
	private int opbrengst = 0;
	
	public ToolshedLocatie(LocatieType type) {
		super(type);
		
		// Toolshedlocatie heeft geen gereedschap nodig
		setVoldoendeGereedschapAanwezig(true);
	}

	@Override
	public int getOpbrengst() {
		return  this.opbrengst;
	}

	@Override
	public void perform(Event event) {
		
		if ( event.getTypeEvent() == EventType.CONTACT ) {
			if (ikBenVeroorzaker(event)) {
				System.out.println("Yes!! dit is de ToolShed!");
			}
		}
		
	}
	
	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
	
	@Override
	public void setOpbrengst(){
		this.opbrengst  = 0;
	}

	@Override
	public boolean isVoldoendeGereedschapAanwezig() {
		return voldoendeGereedschapAanwezig;
	}

	public void setVoldoendeGereedschapAanwezig(boolean voldoendeGereedschapAanwezig) {
			this.voldoendeGereedschapAanwezig = voldoendeGereedschapAanwezig;
	}
	
	@Override
	public void resetGereedschap() {
	}
	
}
