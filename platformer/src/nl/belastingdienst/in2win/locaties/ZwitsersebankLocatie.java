package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gereedschap.GereedschapType;

import com.badlogic.gdx.math.MathUtils;

public class ZwitsersebankLocatie<opbrengst> extends Locatie {

	private int opbrengst;
	final String locatieNaam = "Zwitsersebank" ;
	private int aantalLadders = 0;

	public ZwitsersebankLocatie(LocatieType type) {
		super(type);
	}

	/** 
	 *  Als deze Zwitsersebank is geregistreerd bij de EventBus, dan zal de EventBus 
	 *  bij elke BotstingEvent deze perform -methode aanroepen.
	 */
	@Override
	public void perform(Event event) {
	}
	
	@Override
	public int getOpbrengst(){
		return this.opbrengst;
	}

	@Override
	public String getLocatieNaam(){
		return this.locatieNaam;
	}
	
	@Override
	public void setOpbrengst(){
		int geefGeheelGetal = MathUtils.random(1,3);
		this.opbrengst = LocatieType.ZWITSERSEBANK.getBasisOpbrengst() * geefGeheelGetal;
		
		// verhuisd naar meldbox (want die moet nog weten ..)
		//resetGereedschap();
	}

	public void addLadder() {
		this.aantalLadders++;
		if (aantalLadders >= GereedschapType.LADDER.getAantal()  ) {
			this.setVoldoendeGereedschapAanwezig(true);
		}
	}

	private void setVoldoendeGereedschapAanwezig(boolean voldoendeGereedschapAanwezig) {
			this.voldoendeGereedschapAanwezig = voldoendeGereedschapAanwezig;
	}

	@Override
	public boolean isVoldoendeGereedschapAanwezig() {
		return voldoendeGereedschapAanwezig;
	}
	
	@Override
	public void resetGereedschap() {
		this.aantalLadders = 0;
		this.setVoldoendeGereedschapAanwezig(false);
		System.out.println("gereedschap weer naar 0");
	}
}
