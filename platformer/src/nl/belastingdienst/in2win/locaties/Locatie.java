package nl.belastingdienst.in2win.locaties;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventListener;

public abstract class Locatie implements EventListener {
	LocatieType type;
	Boolean		performActiesJN = false;
	public boolean voldoendeGereedschapAanwezig = false;
	
	public boolean ikBenVeroorzaker(Event event){
		
		if ( this.equals(event.getVeroorzaker()) ) { return true; }  
		return false;
		
	}
	
	public Locatie(LocatieType type) {
		this.type = type;
		
	}

	public LocatieType getType() {
		return type;
	}

	// Basisbedrag per locatie bepalen
	public abstract int getOpbrengst();
	
	public abstract String getLocatieNaam();
	
	public abstract void setOpbrengst();

	public abstract boolean isVoldoendeGereedschapAanwezig();
	
	public abstract void resetGereedschap();
	
}
