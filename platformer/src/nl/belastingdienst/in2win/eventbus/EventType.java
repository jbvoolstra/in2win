package nl.belastingdienst.in2win.eventbus;

public enum EventType {	GESTART, 
							CONTACT, 
							EINDECONTACT, 
							BIJNABIJ, 
							TOETSINGEDRUKT, 
							GEREEDSCHAPGEPAKT, 
							GEREEDSCHAPLOSGELATEN, 
							EINDESPEELTIJD, 
							BEROOFD, 
							ONGELUK, 
							GEWONNEN, 
							VERLOREN};