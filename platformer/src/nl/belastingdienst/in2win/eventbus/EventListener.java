package nl.belastingdienst.in2win.eventbus;

public interface EventListener {

	void perform(Event event);
	
}
