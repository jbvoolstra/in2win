package nl.belastingdienst.in2win.eventbus;

public class Event {

	
	private Object veroorzaker;
	private EventType eventType;
	
	/**
	 * 
	 * @param veroorzaker - mag niet null zijn.
	 */
	public Event(EventType eventType, Object veroorzaker) {
		if ( eventType == null ) {
			throw new IllegalArgumentException("eventType mag niet null zijn!");
		}
		if ( veroorzaker == null ) {
			throw new IllegalArgumentException("veroorzaker mag niet null zijn!");
		}
		this.eventType		= eventType;
		this.veroorzaker 	= veroorzaker;
	}
	
	public EventType getTypeEvent() {
		return eventType;
	}

	public void setTypeEvent(EventType typeEvent) {
		this.eventType = typeEvent;
	}

	public void setVeroorzaker(Object veroorzaker) {
		this.veroorzaker = veroorzaker;
	}

	public Object getVeroorzaker() {
		return this.veroorzaker;
	}
	
}
