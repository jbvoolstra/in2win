package nl.belastingdienst.in2win.eventbus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EventBus {

	private Map<EventType, ArrayList<EventListener>> eventListenerLijstMap = new HashMap<EventType, ArrayList<EventListener>>();

	public EventBus() { 
		
		for(EventType eventType: EventType.values()){
			ArrayList<EventListener> eventListenerLijst = new ArrayList<EventListener>();
			eventListenerLijstMap.put(eventType, eventListenerLijst);
		}
	}

	public void register(EventType eventType, EventListener eventListener) {
		eventListenerLijstMap.get(eventType).add(eventListener);
	}

	public void unregister(EventType eventType, EventListener eventListener) {
		eventListenerLijstMap.get(eventType).remove(eventListener);
	}

	public void fireEvent(Event event) {

		ArrayList<EventListener> eventListenerLijst = this.eventListenerLijstMap.get(event.getTypeEvent());
		for (EventListener eventListener : eventListenerLijst) {
			eventListener.perform(event);
		}
	}
}
