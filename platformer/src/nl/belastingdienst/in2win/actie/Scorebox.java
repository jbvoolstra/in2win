package nl.belastingdienst.in2win.actie;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.locaties.KantoorLocatie;
import nl.belastingdienst.in2win.locaties.Locatie;
import nl.belastingdienst.in2win.locaties.LocatieType;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Scorebox implements EventListener {

	private SpriteBatch gameControllerSpriteBatch;
	private BitmapFont font;
	// FIXME niet meer static!!
	private static int score;
	private static int totaalScore;
	private static int maxBehaaldeScore = 0;

	private String contantOlga;
	private String totaalAfdracht;
	private String maxBehaaldeScoreMelding;
	private static Locatie vorigeLocatie = null;
	private EventBus eventBus;

	public Scorebox(EventBus eventBus) {
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		gameControllerSpriteBatch = new SpriteBatch();

		// registreer in de eventbus
		this.eventBus = eventBus;
		eventBus.register(EventType.CONTACT, this);
		eventBus.register(EventType.GESTART, this);
		eventBus.register(EventType.EINDESPEELTIJD, this);
		eventBus.register(EventType.BEROOFD, this);
		eventBus.register(EventType.ONGELUK, this);
	}

	public void setScoreOpBord(int score, int totaalScore) {
		Scorebox.score = score;
		Scorebox.totaalScore = totaalScore;

	}

	public void update() {
		gameControllerSpriteBatch.begin();
		if (totaalScore == 0) {
			totaalAfdracht = "Er is nog geen belastinggeld af gedragen";
		} else {
			totaalAfdracht = "Er is in het totaal "
					+ Integer.toString(totaalScore)
					+ " Euro aan belastinggeld afgedragen !!";
		}
		font.drawWrapped(gameControllerSpriteBatch, totaalAfdracht, 755, 600,
				250);

		if (this.score == 0) {
			contantOlga = "Olga heeft geen geld bij zich.";
		} else {
			contantOlga = "Olga heeft " + Integer.toString(score)
					+ " Euro bij zich.";
		}
		font.drawWrapped(gameControllerSpriteBatch, contantOlga, 755, 650, 250);

		if (Scorebox.maxBehaaldeScore != 0) {
			maxBehaaldeScoreMelding = "Uw hoogste score tot nu toe is "
					+ Integer.toString(maxBehaaldeScore);
			font.drawWrapped(gameControllerSpriteBatch,
					maxBehaaldeScoreMelding, 755, 550, 250);
		}

		gameControllerSpriteBatch.end();

	}

	@Override
	public void perform(Event event) {
		
		if (EventType.BEROOFD.equals(event.getTypeEvent())) {
			this.score = 0;
		}

		if (EventType.ONGELUK.equals(event.getTypeEvent())) {
			this.score = score / 2;
		}

		if (event.getTypeEvent() == EventType.CONTACT) {

			Object veroorzaker = event.getVeroorzaker();
			if (veroorzaker instanceof Locatie) {
				Locatie loc = (Locatie) veroorzaker;
				if (loc != vorigeLocatie || vorigeLocatie == null) {
					if (loc.voldoendeGereedschapAanwezig) {
						loc.setOpbrengst();
						int opbrengst = loc.getOpbrengst();
						this.score = score + opbrengst;
						setScoreOpBord(score, totaalScore);
						if (loc instanceof KantoorLocatie) {
							KantoorLocatie kantoorLocatie = (KantoorLocatie) loc;
							kantoorLocatie.setAfdracht(score);
							Scorebox.totaalScore = totaalScore + score;
							this.score = 0;
						}
					}
				}
				if (loc.getType() != LocatieType.HEK) {
					vorigeLocatie = loc;
				}
			}
		}

		if (event.getTypeEvent() == EventType.EINDESPEELTIJD) {

			System.out.println("ScoreBox reageert op EINDE SPEELTIJD");
			if (getMaxBehaaldeScore() < getTotaalScore()) {
				setMaxBehaaldeScore(getTotaalScore());
				eventBus.fireEvent(new Event(EventType.GEWONNEN, this));
			} else {
				// volgende fire moet alleen als start geweest
				eventBus.fireEvent(new Event(EventType.VERLOREN, this));
			}
		}

		if (event.getTypeEvent() == EventType.GESTART) {
			this.score = 0;
			Scorebox.totaalScore = 0;
		}
	}

	private int getTotaalScore() {
		return Scorebox.totaalScore;
	}

	public static int getScore() {
		return score;
	}

	public static void setScore(int score) {
		Scorebox.score = score;
	}

	public static int getMaxBehaaldeScore() {
		return maxBehaaldeScore;
	}

	public static void setMaxBehaaldeScore(int maxBehaaldeScore) {
		Scorebox.maxBehaaldeScore = maxBehaaldeScore;
	}

}