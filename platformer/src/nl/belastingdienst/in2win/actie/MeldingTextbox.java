package nl.belastingdienst.in2win.actie;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.locaties.KantoorLocatie;
import nl.belastingdienst.in2win.locaties.Locatie;
import nl.belastingdienst.in2win.locaties.LocatieType;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MeldingTextbox implements EventListener {
	private final String statusPrefix = "Status : ";
	private String meldingTekst;
	private SpriteBatch meldingSpriteBatch;
	private BitmapFont font;
	//private EventBus eventBus;
	private static Locatie vorigeLocatie = null;
	private int afdracht = 0;

	public MeldingTextbox(EventBus eventBus) {
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		this.meldingTekst = statusPrefix;
		meldingSpriteBatch = new SpriteBatch();
		
		// Registreer in de eventbus
		eventBus.register(EventType.CONTACT, this);
		eventBus.register(EventType.BEROOFD, this);
		eventBus.register(EventType.ONGELUK, this);
	}

	public void setMeldingTekst(String melding) {
		this.meldingTekst = statusPrefix + melding;
	}

	public void update() {
		meldingSpriteBatch.begin();
		font.drawWrapped(meldingSpriteBatch, meldingTekst, 755, 720, 250);
		meldingSpriteBatch.end();
	}

	@Override
	public void perform(Event event) {
		String melding = null;
		
		if (EventType.BEROOFD.equals(event.getTypeEvent())) {
			melding = "U bent zojuist beroofd";
			setMeldingTekst(melding);
		}
		
		if (EventType.ONGELUK.equals(event.getTypeEvent())) {
			melding = "U heeft zojuist een ongeluk gehad en bent de helft van uw geld kwijt geraakt";
			setMeldingTekst(melding);
		}
		
		Object veroorzaker = event.getVeroorzaker();
		if (veroorzaker instanceof Locatie) {
			Locatie loc = (Locatie) veroorzaker;

			if (event.getTypeEvent() == EventType.CONTACT) {
				if (loc != vorigeLocatie || vorigeLocatie == null) {
					if (loc.getType() == LocatieType.BELASTINGKANTOOR) {
						KantoorLocatie kantoorLocatie = (KantoorLocatie) loc;
						afdracht = kantoorLocatie.getAfdracht();
						if (afdracht > 0 ) {
						melding = "U bent bij het " + loc.getLocatieNaam()
								+ ", u heeft zojuist "
								+ afdracht
								+ " Euro gestort!";
						} else {
							 melding = "U bent bij het " + loc.getLocatieNaam()
							 +", maar u heeft geen geld om af te dragen";
						}
					} else {
						if (loc.getType() == LocatieType.TOOLSHED) {
							melding = "U bent bij het " + loc.getLocatieNaam();
						} else {
							
						if ( loc.isVoldoendeGereedschapAanwezig() ) {
							melding = "U bent bij het " + loc.getLocatieNaam()
									+ ", u heeft zojuist " + loc.getOpbrengst()
									+ " euro geincaseerd.";
								loc.resetGereedschap();
							} else {
								melding = "U heeft onvoldoende of onjuist gereedschap bij u";
							}
						}
					}
				} else {
					if (loc.getType() != LocatieType.BELASTINGKANTOOR
							&& loc.getType() != LocatieType.TOOLSHED) {
						melding = "Het is niet toegestaan om meerdere keren achter elkaar van "
								+ loc.getLocatieNaam()
								+ " geld te incasseren!!";
					} else {
						melding = "U bezoekt meerdere keren achter elkaar "
								+ loc.getLocatieNaam();
					}
				}
				if (loc.getType() != LocatieType.HEK){
					vorigeLocatie = loc;
					setMeldingTekst(melding);
				}
			}
			
		}
	}
}