package nl.belastingdienst.in2win.actie;

import java.util.HashMap;
import java.util.Map;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.locaties.Locatie;
import nl.belastingdienst.in2win.locaties.LocatieType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

public class MuziekSpeler implements EventListener {

	private static Map<String, Sound> muziekMap = new HashMap<String, Sound>();
	private static Sound actueelMuziekje;
	private static Sound bestolenSound;
	private static Sound gameOverSound;
	private static Sound gewonnenSound;
	private static Sound verlorenSound;
	private static Sound ongelukSound;

	public MuziekSpeler(EventBus eventBus) {

		for (LocatieType locatieType : LocatieType.values()) {
			FileHandle file1 = Gdx.app.getFiles().internal(
					"sounds/" + locatieType.name() + ".mp3");
			if (file1.exists()) {
				muziekMap.put(locatieType.name(), Gdx.audio.newSound(file1));
			}
		}

		FileHandle file2 = Gdx.app.getFiles().internal("sounds/bestolen.mp3");
		if (file2.exists()) {
			bestolenSound = Gdx.audio.newSound(file2);
		} else {
			// fout
		}
		FileHandle file3 = Gdx.app.getFiles().internal("sounds/timeup.mp3");
		if (file3.exists()) {
			gameOverSound = Gdx.audio.newSound(file3);
		} else {
			// fout
		}
		FileHandle file4 = Gdx.app.getFiles().internal("sounds/gameEndWon.mp3");
		if (file4.exists()) {
			gewonnenSound = Gdx.audio.newSound(file4);
		} else {
			// fout
		}
		FileHandle file5 = Gdx.app.getFiles()
				.internal("sounds/gameEndLost.mp3");
		if (file5.exists()) {
			verlorenSound = Gdx.audio.newSound(file5);
		} else {
			// fout
		}
		FileHandle file6 = Gdx.app.getFiles().internal("sounds/ambulance.mp3");
		if (file6.exists()) {
			ongelukSound = Gdx.audio.newSound(file6);
		} else {
			// fout
		}

		eventBus.register(EventType.CONTACT, this);
		eventBus.register(EventType.EINDECONTACT, this);
		eventBus.register(EventType.GEWONNEN, this);
		eventBus.register(EventType.VERLOREN, this);
		eventBus.register(EventType.EINDESPEELTIJD, this);
		eventBus.register(EventType.BEROOFD, this);
		eventBus.register(EventType.ONGELUK, this);
	}

	/**
	 * muziek stoppen.
	 */
	public static void stop() {
		if (actueelMuziekje != null & actueelMuziekje != muziekMap.get(LocatieType.BELASTINGKANTOOR.name())) {
			actueelMuziekje.stop();
		} else {
			// System.out.println("vorig muziekje is null");
		}
	}

	public static void speel(LocatieType type) {

		stop();
		actueelMuziekje = muziekMap.get(type.name());
		if (actueelMuziekje != null) {
			actueelMuziekje.play(1.0f);
		}
	}

	@Override
	public void perform(Event event) {

		if (event.getTypeEvent() == EventType.CONTACT) {
			if (event.getVeroorzaker() instanceof Locatie) {
				Locatie locatie = (Locatie) event.getVeroorzaker();
				LocatieType locatieType = locatie.getType();

				if (locatieType == LocatieType.BELASTINGKANTOOR
						&& Scorebox.getScore() > 0) {
					speel(locatieType);
				} else {
					if (locatieType != LocatieType.BELASTINGKANTOOR) {
						speel(locatieType);
					}
				}
			}
		}

		if (EventType.BEROOFD.equals(event.getTypeEvent())) {
			bestolenSound.play();
		}

		if (EventType.ONGELUK.equals(event.getTypeEvent())) {
			ongelukSound.play();
		}

		if (event.getTypeEvent() == EventType.EINDECONTACT) {
			stop();
		}

		if (event.getTypeEvent() == EventType.EINDESPEELTIJD) {
			stop();
			gameOverSound.play();
		}

		if (event.getTypeEvent() == EventType.VERLOREN) {
			stop();
			verlorenSound.play();
		}

		if (event.getTypeEvent() == EventType.GEWONNEN) {
			stop();
			gewonnenSound.play();
		}

	}
}