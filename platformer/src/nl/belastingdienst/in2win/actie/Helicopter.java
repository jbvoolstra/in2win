package nl.belastingdienst.in2win.actie;

import nl.belastingdienst.in2win.bijnalocaties.BijnaBijKantoorLocatie;
import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;

public class Helicopter implements EventListener {

	private Sound helicopterSound;

	public Helicopter(EventBus eventBus) {
		eventBus.register(EventType.BIJNABIJ, this);
		setHelicopterSound();
	}

	public Sound getHelicopterSound() {
		return this.helicopterSound;
	}

	public void setHelicopterSound() {

		FileHandle file = Gdx.app.getFiles().internal("sounds/helicopter.mp3");
		if (file.exists()) {
			this.helicopterSound = Gdx.audio.newSound(file);
		} else {
			this.helicopterSound = null;
		}
	}

	@Override
	public void perform(Event event) {

		if (event.getVeroorzaker() instanceof BijnaBijKantoorLocatie) {
			if (this.helicopterSound != null) {
				if (MathUtils.random(1, 8) == 5) {
					helicopterSound.play();
				}
			}
		}
	}

}
