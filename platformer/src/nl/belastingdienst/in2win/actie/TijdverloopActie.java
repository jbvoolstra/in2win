package nl.belastingdienst.in2win.actie;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gameSturing.StartKnop;
import nl.belastingdienst.in2win.platformer.BesturingsVeld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TijdverloopActie {
//	private final int 
	private Texture img_clock;
	private BitmapFont font;
	private SpriteBatch klokBatch = new SpriteBatch();

	private static CountDownTimer countDownTimer = new CountDownTimer();
	private static Thread countdownThread;
	private EventBus eventBus;
	private BesturingsVeld besturingsveld;

	/**
	 * 
	 */
	public TijdverloopActie(BesturingsVeld besturingsveld, EventBus eventBus) {
		super();
		img_clock = new Texture(Gdx.files.internal("data/clock_kl.png"));
		font = new BitmapFont();
		countDownTimer.setBeginTijd(120);
		this.eventBus = eventBus;
		this.besturingsveld = besturingsveld;
	}

	/*
	 * Klok met daarin een teller die na x(80) seconden afloopt!
	 */
	public static void startKlok() {
		countdownThread = new Thread(countDownTimer);

		countDownTimer.setBeginTijd(120);
		countdownThread.start();
	}

	/**
	 * de render methode van de timer. teken de klok en werk de tijd bij.
	 */
	public void update() {
		klokBatch.begin();

		klokBatch.draw(img_clock, 850, 430);
		String remaingTime = String.valueOf(countDownTimer.getRemainingTime());
		font.drawWrapped(klokBatch, remaingTime, 880, 420, 250);
		
		if (( countDownTimer.getRemainingTime() <= 0 ) && besturingsveld.getStartKnop().isSpelGestart() ) {
			System.out.println("EINDE SPEELTIJD wordt gefired");
		    eventBus.fireEvent(new Event(EventType.EINDESPEELTIJD, this));
		    besturingsveld.getStartKnop().setSpelGestart(false);
		}
		klokBatch.end();
	}

}
