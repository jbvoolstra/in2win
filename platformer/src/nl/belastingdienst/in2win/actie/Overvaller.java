package nl.belastingdienst.in2win.actie;

import java.util.concurrent.TimeUnit;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.locaties.Locatie;
import nl.belastingdienst.in2win.locaties.OnderwereldLocatie;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

/**
 * De Overvaller weet van de Eventbus wanneer Olga bij een Locatie is
 * (EventType.CONTACT). Per locatie is de kans dat hij durft toe te slaan
 * verschillend. Momenteel slaat hij alleen toe met een kans van 25% als Olga
 * bij de Onderwereld is.
 * 
 * @author jbvoolstra
 * 
 */
public class Overvaller implements EventListener {

	private boolean berooft = false;
	// private int mogelijkTeBerovenBedrag;
	private SpriteBatch overvallerBatch = new SpriteBatch();
	private Texture overvallerPlaatje;
	private boolean toonOvervaller = false;
	private EventBus eb;

	public Overvaller(EventBus eb) {
		this.eb = eb;
		overvallerPlaatje = new Texture(Gdx.files.internal("data/overvaller.gif"));
		
		// registreer in de eventbus
		this.eb.register(EventType.EINDECONTACT, this);
	}

	@Override
	public void perform(Event event) {

		berooft = false;

		if (event.getTypeEvent() == EventType.EINDECONTACT) {

			if (event.getVeroorzaker() instanceof Locatie) {
				Locatie locatie = (Locatie) event.getVeroorzaker();

				if (isBuitHogerDanMinimum()) {

					// beroving kan nu alleen bij contact met de Onderwereld
					// (eenvoudig uit te breiden)
					if (isOnderwereld(locatie)) {
						if (wordtBeroofd()) {
							isOvervallen();
							eb.fireEvent(new Event(EventType.BEROOFD, this));
						}
					}
				}

			}
		}

	}

	private boolean isOnderwereld(Locatie locatie) {
		return locatie instanceof OnderwereldLocatie;
	}

	private boolean wordtBeroofd() {
		//return true;
		return MathUtils.random(1, 4) == 4;
	}

	/**
	 * de 2000 zou variabel kunnen worden bepaald natuurlijk!
	 */
	private boolean isBuitHogerDanMinimum() {
		return Scorebox.getScore() > 2000;
	}

	private void isOvervallen() {
		toonOvervaller = true;

		//declaratie
		Runnable overvaltimer = new Runnable() {

			@Override
			public void run() {
				try {
					TimeUnit.SECONDS.sleep(5);
				} catch (InterruptedException e) {
					toonOvervaller = false;
					e.printStackTrace();
				}
				toonOvervaller = false;
			}
		};
		
		//gebruik
		Thread gebruiktDeTimeout = new Thread(overvaltimer);
		gebruiktDeTimeout.start();
	}

	public void update() {
		if (toonOvervaller) {
			overvallerBatch.begin();
			overvallerBatch.draw(overvallerPlaatje, 100, 140);
			overvallerBatch.end();
		}
	}

	public boolean isBerooft() {
		return berooft;
	}

}
