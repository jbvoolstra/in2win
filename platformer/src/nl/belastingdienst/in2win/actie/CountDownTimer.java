package nl.belastingdienst.in2win.actie;

public class CountDownTimer implements Runnable {

	private int seconds = 20;
	private boolean suspend = false;

	@Override
	public void run() {
		while (seconds > 0) {
		    try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		    if (!suspend) {
			    seconds--;
			}
		}
	}
	
	public void suspend() {
		this.suspend = true;
	}

	public void resume() {
		this.suspend = false;
	}
	
	public int getRemainingTime() {
		return this.seconds;
	}

	public void setBeginTijd(int secs) {
		this.seconds = secs;
	}
}
