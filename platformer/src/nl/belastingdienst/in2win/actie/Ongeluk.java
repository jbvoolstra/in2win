package nl.belastingdienst.in2win.actie;

import java.util.concurrent.TimeUnit;

import nl.belastingdienst.in2win.bijnalocaties.BijnaBijLocatie;
import nl.belastingdienst.in2win.bijnalocaties.BijnaBijZwitsersebankLocatie;
import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.platformer.GameController;
import nl.belastingdienst.in2win.platformer.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

/**
 * De ongeluk weet van de Eventbus wanneer Olga bij een Locatie is
 * (EventType.CONTACT). Per locatie is de kans dat hij durft toe te slaan
 * verschillend. Momenteel slaat hij alleen toe met een kans van 25% als Olga
 * bij de Onderwereld is.
 * 
 * @author jbvoolstra
 * 
 */
public class Ongeluk implements EventListener {

	private static SpriteBatch ongelukBatch = new SpriteBatch();
	private static Texture ziekenhuisbedPlaatje;
	private boolean toonOngeluk = false;
	private EventBus eb;
	private GameController gameController;

	public Ongeluk(EventBus eventBus, GameController gameController) {
		this.eb = eventBus;
		this.gameController = gameController;
		ziekenhuisbedPlaatje = new Texture(
				Gdx.files.internal("data/ziekenhuisbed.png"));

		// registreer in de eventbus
		eventBus.register(EventType.BIJNABIJ, this);
	}

	@Override
	public void perform(Event event) {

		if (event.getTypeEvent() == EventType.BIJNABIJ) {

			if (event.getVeroorzaker() instanceof BijnaBijLocatie) {
				BijnaBijLocatie locatie = (BijnaBijLocatie) event
						.getVeroorzaker();

				if (isZwitsersebank(locatie)) {

					// ongeluk kan nu bij elk contact -event optreden
					// (eenvoudig uit te breiden / of specifieker te maken)
					if (krijgtOngeluk()) {
						heeftOngeluk();
						eb.fireEvent(new Event(EventType.ONGELUK, this));
					}
				}
			}
		}

	}

	private boolean isZwitsersebank(BijnaBijLocatie locatie) {
		return locatie instanceof BijnaBijZwitsersebankLocatie;
	}

	private void heeftOngeluk() {
		toonOngeluk = true;

		// declaratie
		Runnable ongeluktimer = new Runnable() {

			@Override
			public void run() {
				try {
					TimeUnit.SECONDS.sleep(5);
				} catch (InterruptedException e) {
					toonOngeluk = false;
					e.printStackTrace();
				}
				toonOngeluk = false;
			}
		};

		// gebruik
		Thread gebruiktDeTimeout = new Thread(ongeluktimer);
		gebruiktDeTimeout.start();
	}

	private boolean krijgtOngeluk() {
		// return true;
		return MathUtils.random(1, 7) == 7;
	}

	public void update() {

		if (toonOngeluk) {

			Player player = gameController.getSpeelVeld().getPlayer();
			float x = player.getPosition().x * 30;
			float y = player.getPosition().y * 30;

			ongelukBatch.begin();
			ongelukBatch.draw(ziekenhuisbedPlaatje, x, y);
			ongelukBatch.end();
		}
	}

}
