package nl.belastingdienst.in2win.platformer;

import java.util.HashMap;
import java.util.Map;

import nl.belastingdienst.in2win.locaties.*;
import nl.belastingdienst.in2win.actie.Helicopter;
import nl.belastingdienst.in2win.actie.Ongeluk;
import nl.belastingdienst.in2win.actie.Overvaller;
import nl.belastingdienst.in2win.bijnalocaties.*;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.gameSturing.StartKnop;

import org.jsoup.nodes.Element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public class SpeelVeld {
	private Map<String, Locatie> locaties = new HashMap<String, Locatie>();
	private Map<String, BijnaBijLocatie> bijnaBijLocaties = new HashMap<String, BijnaBijLocatie>();
	private SchermOpbouw schermOpbouw;
	private Sprite opgebouwdScherm;
	private World world;
	private Vector2 playerOrigin;
	private ContactAfhandeling contacten;
	private Player player;
	private SpriteBatch speelveldSpriteBatch = new SpriteBatch();
	private EventBus eventBus;
	private GameController gameController;
	private Overvaller overvaller;
	private Ongeluk ongeluk;
	private Helicopter helicopter;
	

	/**
	 * @param world
	 * @param gameController
	 */
	public SpeelVeld(World world, GameController gameController, EventBus eb) {
		this.world = world;
		this.eventBus = eb;
		this.gameController = gameController;
		
		overvaller = new Overvaller(eventBus);
		ongeluk = new Ongeluk(eventBus, gameController);
		helicopter = new Helicopter(eventBus);
		
		initLocaties();
		initBijnaBijLocaties();
		initContacten();
		initSchermOpbouw("data/world");
		player = new Player(gameController, world, this, eb);

	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * @param scherm
	 * @return
	 */
	public Sprite initSchermOpbouw(String scherm) {
		schermOpbouw = new SchermOpbouw(scherm, world, this);
		opgebouwdScherm = schermOpbouw.loadGeometry();
		playerOrigin = schermOpbouw.getPlayerOrigin();
		return opgebouwdScherm;
	}

	private void initLocaties() {
		locaties.put(LocatieType.BELASTINGKANTOOR.name(), new KantoorLocatie(
				LocatieType.BELASTINGKANTOOR));
		locaties.put(LocatieType.TOOLSHED.name(), new ToolshedLocatie(
				LocatieType.TOOLSHED));
		locaties.put(LocatieType.BELASTINGPARADIJS.name(),
				new BelastingParadijsLocatie(LocatieType.BELASTINGPARADIJS));
		locaties.put(LocatieType.BRAVEBURGER.name(), new BraveBurgerLocatie(
				LocatieType.BRAVEBURGER));
		locaties.put(LocatieType.ONDERWERELD.name(), new OnderwereldLocatie(
				LocatieType.ONDERWERELD));
		locaties.put(LocatieType.ZWITSERSEBANK.name(),
				new ZwitsersebankLocatie(LocatieType.ZWITSERSEBANK));
		locaties.put(LocatieType.HEK.name(), new HekLocatie(LocatieType.HEK));
	}

	private void initBijnaBijLocaties() {
		bijnaBijLocaties.put(BijnaBijLocatieType.BIJNABIJBELASTINGKANTOOR
				.name(), new BijnaBijKantoorLocatie(
				BijnaBijLocatieType.BIJNABIJBELASTINGKANTOOR));
		bijnaBijLocaties.put(BijnaBijLocatieType.BIJNABIJTOOLSHED.name(),
				new BijnaBijToolshedLocatie(
						BijnaBijLocatieType.BIJNABIJTOOLSHED));
		bijnaBijLocaties.put(BijnaBijLocatieType.BIJNABIJBELASTINGPARADIJS
				.name(), new BijnaBijBelastingParadijsLocatie(
				BijnaBijLocatieType.BIJNABIJBELASTINGPARADIJS));
		bijnaBijLocaties.put(BijnaBijLocatieType.BIJNABIJBRAVEBURGER.name(),
				new BijnaBijBraveBurgerLocatie(
						BijnaBijLocatieType.BIJNABIJBRAVEBURGER));
		bijnaBijLocaties.put(BijnaBijLocatieType.BIJNABIJONDERWERELD.name(),
				new BijnaBijOnderwereldLocatie(
						BijnaBijLocatieType.BIJNABIJONDERWERELD));
		bijnaBijLocaties.put(BijnaBijLocatieType.BIJNABIJZWITSERSEBANK.name(),
				new BijnaBijZwitsersebankLocatie(
						BijnaBijLocatieType.BIJNABIJZWITSERSEBANK));
	}

	private void initContacten() {
		this.contacten = new ContactAfhandeling(eventBus);
		this.world.setContactListener(contacten);
	}

	/**
	 * @param path
	 * @param box
	 */
	public void addLocatie(Element path, Body box) {
		String type = path.attr("LocatieType");
		if (type != null && type != "") {
			// Koppel userdate en locatie.
			Locatie locatie = locaties.get(type);
			if (locatie != null) {
				box.setUserData(locatie);
			} 
		}
	}

	public void addBijnaLocatie(Element path, Body box) {
		String type = path.attr("LocatieType");
		if (type != null && type != "") {
			// Koppel userdate en locatie.
			BijnaBijLocatie bijnaBijLocatie = bijnaBijLocaties.get("BIJNABIJ"
					+ type);
			box.setUserData(bijnaBijLocatie);
		}
	}

	/**
	 * @return
	 */
	public Vector2 getPlayerOrigin() {
		return playerOrigin;
	}

	/**
	 * @param camera
	 */
	public void renderSpeelveld(OrthographicCamera camera) {

		// step the world
		if (gameController.getBesturingsVeld().getStartKnop().isSpelGestart()) {
			world.step(Gdx.graphics.getDeltaTime(), 4, 4);
		}

		speelveldSpriteBatch.setProjectionMatrix(camera.combined);

		// tekenen van achtergrond en speler
		speelveldSpriteBatch.begin();
			opgebouwdScherm.draw(speelveldSpriteBatch);
			player.renderPlayer(camera, speelveldSpriteBatch);
		speelveldSpriteBatch.end();
		
		// overvaller plaatje
		overvaller.update();
		// ongeluk plaatje
		ongeluk.update();
		// tekenen van ladder(s) bij de zwitsersebank
		gameController.getLadder().updateLadderBijDeZwitserseBank();
		// ladder bij Olga drawen
		gameController.getLadder().updateLadderBijOlga();
		// tekenen van boot(s) bij de zwitsersebank
		gameController.getBoot().updateBootBijHetBelastingParadijs();
		// boot bij Olga drawen
		gameController.getBoot().updateBootBijOlga();
		// tekenen van schepje(s) bij de zwitsersebank
		gameController.getSchepje().updateSchepjeBijDeOnderwereld();
		// schepje bij Olga drawen
		gameController.getSchepje().updateSchepjeBijOlga();
	}
}
