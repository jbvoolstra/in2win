package nl.belastingdienst.in2win.platformer;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gameSturing.Knop;
import nl.belastingdienst.in2win.gameSturing.KnopType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;

public class InputAfhandeling implements InputProcessor {

	private EntryPoint entrypoint;
	private World world;
	protected Body hitBody = null;
	protected OrthographicCamera camera;
	private Vector3 testPoint;
	private QueryCallback callback;

	/**
	 * @param game
	 */
	public InputAfhandeling(EntryPoint game, World world) {
		Gdx.input.setInputProcessor(this);
		this.entrypoint = game;
		this.camera = game.getCamera();
		this.world = world;

		// /** we instantiate this vector and the callback here so we don't
		// irritate the GC **/
		this.testPoint = new Vector3();
		this.callback = new QueryCallback() {
			@Override
			public boolean reportFixture(Fixture fixture) {
				// if the hit point is inside the fixture of the body
				// we report it
				if (fixture.testPoint(testPoint.x, testPoint.y)) {
					hitBody = fixture.getBody();
					return false;
				} else
					return true;
			}
		};
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		switch (keycode) {
		case Input.Keys.LEFT:
			// game.move(-1);
			return true;
		case Input.Keys.RIGHT:
			// game.move(1);
			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		// game.move(0);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.InputProcessor#keyTyped(char)
	 */
	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.InputProcessor#touchDown(int, int, int, int)
	 */
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		// translate the mouse coordinates to world coordinates
		this.camera.unproject(testPoint.set(screenX, screenY, 0));

		// ask the world which bodies are within the given
		// bounding box around the mouse pointer
		hitBody = null;
		try {
			world.QueryAABB(callback, testPoint.x - 0.0001f,testPoint.y - 0.0001f, testPoint.x + 0.0001f,testPoint.y + 0.0001f);
		} catch (NullPointerException np) {	}

		// if we hit something we create a new mouse joint
		// and attach it to the hit body. (ignore Kinematic Bodies)
		// if (hitBody != null && hitBody.getType() != BodyType.KinematicBody) {
		if (hitBody != null) {
			
			try {
				Object o = hitBody.getFixtureList().get(0).getBody().getUserData();
				if (o instanceof Knop) {
					Knop knop = (Knop) o;
					entrypoint.eventBus.fireEvent(new Event(EventType.TOETSINGEDRUKT,knop));
				}
			
			} catch (NullPointerException np) {
				System.out.println("(catch nullpointer) hitBody = "
						+ hitBody.getFixtureList().get(0).getType().toString());
				System.out.println("(catch nullpointer) hitBody = "
						+ hitBody.getFixtureList().get(0).getClass());
			}
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.InputProcessor#touchUp(int, int, int, int)
	 */
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.InputProcessor#touchDragged(int, int, int)
	 */
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.InputProcessor#mouseMoved(int, int)
	 */
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.InputProcessor#scrolled(int)
	 */
	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
