package nl.belastingdienst.in2win.platformer;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class SchermOpbouw {

	private static float SCALE = 0.01f;
	private World world;
	private int height;
	private int width;
	private Sprite background;
	private String scherm;
	private Vector2 playerOrigin;
	private Vector2 rondeStartButton;

	private SpeelVeld speelveld;
	private BesturingsVeld besturingsVeld;

	public SchermOpbouw(String scherm, World world) {
		this.scherm = scherm;
		this.world = world;
	}

	public SchermOpbouw(String scherm, World world, SpeelVeld speelVeld) {
		this.scherm = scherm;
		this.world = world;
		this.speelveld = speelVeld;
	}

	public SchermOpbouw(String scherm, World world,
			BesturingsVeld besturingsVeld) {
		this.scherm = scherm;
		this.world = world;
		this.besturingsVeld = besturingsVeld;
	}

	/**
	 * @return
	 */
	public Vector2 getPlayerOrigin() {
		return playerOrigin;
	}

	public Vector2 getrondeStartButton() {
		return rondeStartButton;
	}

	/**
	 * @return
	 */
	public Sprite loadGeometry() {
		Document doc = leesXml();
		// Get the world dimensions
		Element svg = doc.child(0);
		width = Integer.parseInt(svg.attr("width"));
		height = Integer.parseInt(svg.attr("height"));

		Gdx.app.log("level", "width: " + width);

		Element objectLayer = doc.select("#objects").first();

		// speelveld
		if (objectLayer != null) {

			// get all objects
			Vector2 baseTransform = parseSVGTransform(objectLayer
					.attr("transform"));

			Gdx.app.log("WorldLoader", "v: " + baseTransform.toString());

			for (Element rect : objectLayer.select("rect")) {
				initRectangle(baseTransform, rect);
			}

			for (Element path : objectLayer.select("path")) {
				initCircle(baseTransform, path);
			}

			if (this.scherm == "data/world") {
				// Load the player origin
				Element originRect = doc.select("#player-origin rect").first();
				playerOrigin = new Vector2(Float.parseFloat(originRect
						.attr("x")) * SCALE, Float.parseFloat(originRect
						.attr("y")) * SCALE);
			}
		}

		loadBitmaps();
		return background;

	}

	private void loadBitmaps() {
		Texture backgroundTexture = new Texture(Gdx.files.internal(scherm
				+ ".png"));
		background = new Sprite(backgroundTexture, width, height);
		if (scherm.equals("data/besturingsVeld")) {
			background.setOrigin((float) 20.6, 0);
		} else {
			background.setOrigin((float) 0, 0);
		}
		background.setScale(SCALE, SCALE);
	}

	private Document leesXml() {
		FileHandle handle = Gdx.files.internal(scherm + ".svg");
		Document doc = null;

		try {
			doc = Jsoup.parse(handle.read(), "utf-8", "", Parser.xmlParser());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}

	private void initRectangle(Vector2 baseTransform, Element rect) {
		float xPositie = Float.parseFloat(rect.attr("x"));
		float yPositie = Float.parseFloat(rect.attr("y"));
		float boxwidth = Float.parseFloat(rect.attr("width"));
		float boxheight = Float.parseFloat(rect.attr("height"));

		// transform from top-left to bottom-left origin
		yPositie = this.height - yPositie - boxheight;

		// get svg transform offset
		Vector2 tr = parseSVGTransform(rect.attr("transform"));

		Body box = WorldUtil.createBox(world, BodyType.StaticBody, 0.5f
				* boxwidth * SCALE, 0.5f * boxheight * SCALE, 0f);
		box.setTransform((baseTransform.x - tr.x + xPositie + boxwidth * 0.5f)
				* SCALE, (baseTransform.y - tr.y + yPositie + boxheight * 0.5f)
				* SCALE, 0f);

		speelveld.addLocatie(rect, box);

	}

	private void initCircle(Vector2 baseTransform, Element path) {
		float x;
		float y;
		if (path.attr("sodipodi:type").equals("arc")) {

			Vector2 tr;
			int afwijking;
			String type0 = path.attr("KnopType");
			if (type0 != null && type0 != "") {
				afwijking = 2048;
				tr = parseSVGTransformMatrix(path.attr("transform"));
			} else {
				afwijking = 0;
				tr = parseSVGTransform(path.attr("transform"));
			}

			x = Float.parseFloat(path.attr("sodipodi:cx")) + afwijking;
			y = -Float.parseFloat(path.attr("sodipodi:cy")) + height;

			Body box = WorldUtil.createCircle(world, BodyType.StaticBody,
					Float.parseFloat(path.attr("sodipodi:rx")) * SCALE, 0f);
			box.setTransform((baseTransform.x + tr.x + x) * SCALE,
					(baseTransform.y - tr.y + y) * SCALE, 0f);

			String type1 = path.attr("KnopType");
			if (type1 != null && type1 != "") {
				// Koppel userdate en locatie.
				besturingsVeld.addKnop(path, box);
				if ( type1 == "START" ) {
					rondeStartButton = baseTransform;	
				} 
			}

			String type2 = path.attr("LocatieType");
			if (type2 != null && type2 != "") {
				
				// Koppel userdate en locatie.
				speelveld.addLocatie(path, box);
				
				// Creëer bijna Contact ruime (locatiecirkel + ruimte
				Body box1 = WorldUtil.createCircle(world, BodyType.StaticBody,((Float.parseFloat(path.attr("sodipodi:rx")) + (Float.parseFloat(path.attr("sodipodi:rx")) / 3)) * SCALE),0.6f);
				box1.setTransform((baseTransform.x + tr.x + x) * SCALE, (baseTransform.y - tr.y + y) * SCALE, 0.6f);
				box1.getFixtureList().get(0).setSensor(true);
				speelveld.addBijnaLocatie(path, box1);
			}
		}
	}

	private Vector2 parseSVGTransform(String transform) {
		transform = transform.trim();
		if (transform.equals("")) {
			return new Vector2(0f, 0f);
		}

		if (!transform.startsWith("translate(") || !transform.endsWith(")")) {
			return null;
		}
		transform = transform.substring("translate(".length(),
				transform.length() - 1);
		String parts[] = transform.split(",");
		if (parts.length != 2)
			return null;

		return new Vector2(Float.parseFloat(parts[0].trim()),
				Float.parseFloat(parts[1].trim()));
	}
	
	private Vector2 parseSVGTransformMatrix(String transform) {
		transform = transform.trim();
		if (transform.equals("")) {
			return new Vector2(0f, 0f);
		}

		if (!transform.startsWith("matrix(") || !transform.endsWith(")")) {
			return null;
		}
		transform = transform.substring("matrix(".length(),
				transform.length() - 1);
		String parts[] = transform.split(",");
		//if (parts.length != 2) {
		//	return new Vector2(Float.parseFloat(parts[0].trim()),Float.parseFloat(parts[1].trim()));
		//}
		
		return new Vector2(0f, 0f);
	}

	public Vector2 getRondeStartButton() {
		return rondeStartButton;
	}

}
