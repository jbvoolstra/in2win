package nl.belastingdienst.in2win.platformer;

import com.badlogic.gdx.math.MathUtils;

/**
 * @author boevj01 
 * Klasse wordt gebruikt als Randomizer functie voor het spel Olga.
 * Is gemaakt als "Singleton" (Static methode).
 * Aan te roepen als;
 * 		GeefWillekeurigGetal getal = GeefWillekeurigGetal.getInstance();  
 * Voor nu 2 mogelijkheden
 * @param int begin, int eind
 * 		geeft een geheel getal terug tussen deze parameters.
 * @param float begin, float eind
 * 		geeft een decimaal getal terug tussen deze parameters.
 * 
 */
public class GeefWillekeurigGetal {
	private static int geheelGetal;
	private static float decimaalGetal;
	private static GeefWillekeurigGetal geefWillekeurigGetal;

	public static GeefWillekeurigGetal getInstance() {
		if (geefWillekeurigGetal == null) {
			geefWillekeurigGetal = new GeefWillekeurigGetal();
		}
		return geefWillekeurigGetal;
	}

	public int geefGeheelGetal(int begin, int eind) {
		geheelGetal = MathUtils.random(begin, eind);
		return geheelGetal;
	}

	public float geefDecimaalGetal(float begin, float eind) {
		decimaalGetal = MathUtils.random(begin, eind);
		return decimaalGetal;
	}
}
