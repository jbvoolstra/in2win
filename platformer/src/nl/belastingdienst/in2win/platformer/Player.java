package nl.belastingdienst.in2win.platformer;

import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gereedschap.GereedschapType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.World;

public class Player implements EventListener {

	final static float MAX_VELOCITY = 7f;

	private Body body;
	private Sprite olgaSprite;
	private World world;
	private float scale;
	private GereedschapType gereedschapType = null;
	private int geldContant = 0;
	private int geldAfgestaan = 0;
	private boolean magLadderMeenemen;
	private boolean magBootMeenemen;
	private boolean magSchepjeMeenemen;

	
	public Player(GameController gameController, World world,
			SpeelVeld speelveld, EventBus eb) {
		this.world = world;
		this.scale = gameController.getScale();
		createPlayer();
		initSprite();

		Vector2 origin = speelveld.getPlayerOrigin();

		body.setTransform(origin.x, origin.y, 0);
		body.setFixedRotation(true);

		eb.register(EventType.CONTACT, this);

	}

	private void initSprite() {
		// Laad texture in en maak er een sprite van
		Texture texture = new Texture(Gdx.files.internal("data/Icon_olga.png"));

		TextureRegion region = new TextureRegion(texture, 0, 0,
				texture.getWidth(), texture.getHeight());
		olgaSprite = new Sprite(region);
		// Transformeer de grootte van de box naar game units (1x1)
		scale = 1 / olgaSprite.getWidth();
		olgaSprite.setScale(scale, scale);
	}

	private void createPlayer() {
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		body = world.createBody(def);

		CircleShape circle = new CircleShape();
		circle.setRadius(0.45f);
		body.createFixture(circle, 0);
		circle.dispose();

		body.setBullet(true);
		body.setUserData(this);
	}

	/**
	 * @return
	 */
	public Body getBody() {
		return body;
	}

	/**
	 * @param cam
	 * @param batch
	 */
	public void renderPlayer(OrthographicCamera cam, SpriteBatch batch) {
		olgaSprite.draw(batch);
		updatePosition();
		updateImage();
	}

	private void updateImage() {
		Vector2 v = body.getPosition();
		float w = olgaSprite.getWidth();
		float h = olgaSprite.getHeight();
		olgaSprite.setPosition(v.x - 0.5f * w, v.y - 0.5f * h);
	}

	private void updatePosition() {
		Vector2 vel = body.getLinearVelocity();
		Vector2 pos = body.getPosition();

		// cap max velocity on x
		if (Math.abs(vel.x) > MAX_VELOCITY) {
			vel.x = Math.signum(vel.x) * MAX_VELOCITY;
			body.setLinearVelocity(vel.x, vel.y);
		}
		if (Math.abs(vel.y) > MAX_VELOCITY) {
			vel.y = Math.signum(vel.y) * MAX_VELOCITY;
			body.setLinearVelocity(vel.x, vel.y);
		}

		// apply left impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Keys.LEFT) && vel.x > -MAX_VELOCITY) {
			body.applyLinearImpulse(-2f, 0, pos.x, pos.y);
			body.setTransform(pos, MathUtils.PI);
		}

		// apply right impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Keys.RIGHT) && vel.x < MAX_VELOCITY) {
			body.applyLinearImpulse(2f, 0, pos.x, pos.y);
			body.setTransform(pos, 0f);
		}

		// apply left impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Keys.UP) && vel.y < MAX_VELOCITY) {
			body.applyLinearImpulse(0, 2f, pos.x, pos.y);
			body.setTransform(pos, 0.5f * MathUtils.PI);
		}

		// apply right impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Keys.DOWN) && vel.y > -MAX_VELOCITY) {
			body.applyLinearImpulse(0, -2f, pos.x, pos.y);
			body.setTransform(pos, 1.5f * MathUtils.PI);
		}

		body.setAwake(true);
	}

	/**
	 * @return
	 */
	public Vector2 getPosition() {
		return body.getPosition();
	}

	/**
	 * @param keycode
	 * @return
	 */
	public boolean keyDown(int keycode) {
		return false;
	}

	/**
	 * @param keycode
	 * @return
	 */
	public boolean keyUp(int keycode) {
		return false;
	}

	/**
	 * @return
	 */
	public int getGeldContant() {
		return geldContant;
	}

	/**
	 * @param geldContant
	 */
	public void setGeldContant(int geldContant) {
		this.geldContant = geldContant;
	}

	/**
	 * @return
	 */
	public int getGeldAfgestaan() {
		return geldAfgestaan;
	}

	/**
	 * @param geldAfgestaan
	 */
	public void setGeldAfgestaan(int geldAfgestaan) {
		this.geldAfgestaan = geldAfgestaan;
	}

	@Override
	public void perform(Event event) {

		if (event.getTypeEvent() == EventType.CONTACT) {

			// uitgesterd omdat veroorzaker bij de locatie als veroorzaker wordt
			// meegegeven.
			// if (event.getVeroorzaker() == this ) {
			getBody().setLinearVelocity(0, 0);
			getBody().setAngularVelocity(0);
			getBody().setLinearDamping(0);
			// }
		}

	}

	public GereedschapType getGereedschapType() {
		return this.gereedschapType;
	}

	public void setGereedschapType(GereedschapType gereedschapType) {
		this.gereedschapType = gereedschapType;
		// Olga mag een/moet een stuk gereedschap meenemen
		if (getGereedschapType() == gereedschapType.LADDER) {
			magLadderMeenemen = true;
		}
		if (getGereedschapType() == gereedschapType.BOOT) {
			magBootMeenemen = true;
		}
		if (getGereedschapType() == gereedschapType.SCHEP) {
			magSchepjeMeenemen = true;
		}
	}

	public void resetGereedschapType() {
		if (getGereedschapType() == gereedschapType.LADDER) {
			magLadderMeenemen = false;
		}
		if (getGereedschapType() == gereedschapType.BOOT) {
			magBootMeenemen = false;
		}
		if (getGereedschapType() == gereedschapType.SCHEP) {
			magSchepjeMeenemen = false;
		}
		this.gereedschapType = null;
	}

	public boolean isMagLadderMeenemen() {
		return magLadderMeenemen;
	}
	
	public boolean isMagBootMeenemen() {
		return magBootMeenemen;
	}
	
	public boolean isMagSchepjeMeenemen() {
		return magSchepjeMeenemen;
	}
}
