package nl.belastingdienst.in2win.platformer;

import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.gereedschap.Boot;
import nl.belastingdienst.in2win.gereedschap.Ladder;
import nl.belastingdienst.in2win.gereedschap.Schepje;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * @author NIMWJ00, date 11-02-2013
 * 
 */
public class GameController {

	private static float SCALE = 0.01f;
	private World world;
	private SpeelVeld speelVeld;
	private BesturingsVeld besturingsVeld;
	private UitlegVeld uitlegVeld;
	private Ladder ladder = null;
	private Schepje schepje = null;
	private Boot boot = null;

	public GameController() {
		this.world = new World(new Vector2(0, 0), true);
	}

	/**
	 * 
	 */
	public void maakUitlegVeld(EventBus eb) {
		uitlegVeld = new UitlegVeld(world, this, eb);
	}

	/**
	 * 
	 */
	public void maakSpeelveld(EventBus eventBus) {
		this.speelVeld = new SpeelVeld(world, this, eventBus);

	}

	/**
	 * 
	 */
	public void maakBesturingsveld(EventBus eventBus) {
		this.besturingsVeld = new BesturingsVeld(world, this, eventBus);
	}

	public void maakLadder(EventBus eventBus) {
		this.ladder = new Ladder(eventBus, this);
	}

	public void maakSchepje(EventBus eventBus) {
		this.schepje = new Schepje(eventBus, this);
	}

	public void maakBoot(EventBus eventBus) {
		this.boot = new Boot(eventBus, this);
	}
	public Ladder getLadder() {
		return ladder;
	}
	public Schepje getSchepje() {
		return schepje;
	}
	public Boot getBoot() {
		return boot;
	}

	/**
	 * @param camera
	 */
	public void render(OrthographicCamera camera, EventBus eventBus) {
		speelVeld.renderSpeelveld(camera);
		besturingsVeld.renderBesturingsVeld(camera);
		uitlegVeld.renderUitlegVeld(camera);
	}

	public float getScale() {
		return SCALE;
	}

	public BesturingsVeld getBesturingsVeld() {
		return besturingsVeld;
	}

	public SpeelVeld getSpeelVeld() {
		return speelVeld;
	}

	public World getWorld() {
		return world;
	}
}
