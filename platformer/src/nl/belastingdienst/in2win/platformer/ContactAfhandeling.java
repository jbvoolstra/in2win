package nl.belastingdienst.in2win.platformer;

import nl.belastingdienst.in2win.bijnalocaties.BijnaBijLocatie;
import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.locaties.Locatie;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class ContactAfhandeling implements ContactListener {

	private EventBus eventBus;

	public ContactAfhandeling(EventBus eb) {
		super();
		this.eventBus = eb;
	}
	
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#beginContact(com.badlogic.gdx.physics.box2d.Contact)
	 */
	@Override
	public void beginContact(Contact contact) {
		Object objectA = contact.getFixtureA().getBody().getUserData();
		Object objectB = contact.getFixtureB().getBody().getUserData();

		Player player = geefPlayer(objectA, objectB);
		Locatie locatie = geefLocatie(objectA, objectB);

		if (player != null && locatie != null) {
			
			//
			eventBus.fireEvent(new Event(EventType.CONTACT, locatie));
		}	
	}
		
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#endContact(com.badlogic.gdx.physics.box2d.Contact)
	 */
	@Override
	public void endContact(Contact contact) {
		Object objectA = contact.getFixtureA().getBody().getUserData();
		Object objectB = contact.getFixtureB().getBody().getUserData();

		//Player player = geefPlayer(objectA, objectB);
		Locatie locatie = geefLocatie(objectA, objectB);
		if ( locatie != null ) {	
			eventBus.fireEvent(new Event(EventType.EINDECONTACT, locatie));
		}
		
		BijnaBijLocatie bijnaBijLocatie = geefBijnaBijLocatie(objectA, objectB);
		if ( bijnaBijLocatie != null ) {	
			eventBus.fireEvent(new Event(EventType.BIJNABIJ, bijnaBijLocatie));
		}
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#preSolve(com.badlogic.gdx.physics.box2d.Contact, com.badlogic.gdx.physics.box2d.Manifold)
	 */
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#postSolve(com.badlogic.gdx.physics.box2d.Contact, com.badlogic.gdx.physics.box2d.ContactImpulse)
	 */
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
	}
	
	private Player geefPlayer(Object objectA, Object objectB) {
		boolean objectAIsPlayer = objectA instanceof Player;
		boolean objectBIsPlayer = objectB instanceof Player;
		
		Player player = null;

		if (objectAIsPlayer) {
			player = (Player) objectA;
		} else if (objectBIsPlayer) {
			player = (Player) objectB;
		}
		return player;
	}

	private Locatie geefLocatie(Object objectA, Object objectB) {
		boolean objectAIsLocatie = objectA instanceof Locatie;
		boolean objectBIsLocatie = objectB instanceof Locatie;
		
		Locatie locatie = null;

		if (objectAIsLocatie) {
			locatie = (Locatie) objectA;
		} else if (objectBIsLocatie) {
			locatie = (Locatie) objectB;
		}
		return locatie;
	}
	
	private BijnaBijLocatie geefBijnaBijLocatie(Object objectA, Object objectB) {
		boolean objectAIsLocatie = objectA instanceof BijnaBijLocatie;
		boolean objectBIsLocatie = objectB instanceof BijnaBijLocatie;
		
		BijnaBijLocatie bijnaBijLocatie = null;

		if (objectAIsLocatie) {
			bijnaBijLocatie = (BijnaBijLocatie) objectA;
		} else if (objectBIsLocatie) {
			bijnaBijLocatie = (BijnaBijLocatie) objectB;
		}
		return bijnaBijLocatie;
	}
}
