package nl.belastingdienst.in2win.platformer;

import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.gameSturing.StartKnop;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;

public class UitlegVeld {

	private SchermOpbouw schermOpbouw;
	private Sprite opgebouwdScherm;
	private SpriteBatch uitlegVeldSpriteBatch = new SpriteBatch();
	private World world;
	private GameController gameController;

	public UitlegVeld(World world, GameController gameController, EventBus eventbus) {
		this.world = world;
		this.gameController = gameController;
		initSchermOpbouw("data/uitlegVeld");
	}

	/**
	 * @param scherm
	 * @return
	 */
	public Sprite initSchermOpbouw(String scherm) {
		schermOpbouw = new SchermOpbouw(scherm, world);
		opgebouwdScherm = schermOpbouw.loadGeometry();
		return opgebouwdScherm;
	}

	/**
	 * @param camera
	 */
	public void renderUitlegVeld(OrthographicCamera camera) {
		// step the world
		if (!gameController.getBesturingsVeld().getStartKnop().isSpelGestart()){
		// volgende uitgesterde regel gooide de start in de knoei	
		//	world.step(Gdx.graphics.getDeltaTime(), 4, 4);
		
			uitlegVeldSpriteBatch.setProjectionMatrix(camera.combined);
		
			uitlegVeldSpriteBatch.begin();
				opgebouwdScherm.draw(uitlegVeldSpriteBatch);
			uitlegVeldSpriteBatch.end();
		}
		
	}
	
}