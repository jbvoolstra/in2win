package nl.belastingdienst.in2win.platformer;

import java.util.HashMap;
import java.util.Map;

import nl.belastingdienst.in2win.actie.MeldingTextbox;
import nl.belastingdienst.in2win.actie.Scorebox;
import nl.belastingdienst.in2win.actie.TijdverloopActie;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.gameSturing.BootKnop;
import nl.belastingdienst.in2win.gameSturing.Knop;
import nl.belastingdienst.in2win.gameSturing.KnopType;
import nl.belastingdienst.in2win.gameSturing.LadderKnop;
import nl.belastingdienst.in2win.gameSturing.PauzeKnop;
import nl.belastingdienst.in2win.gameSturing.SchepjeKnop;
import nl.belastingdienst.in2win.gameSturing.StartKnop;
import nl.belastingdienst.in2win.gameSturing.StopKnop;

import org.jsoup.nodes.Element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public class BesturingsVeld {

	private Map<String, Knop> knoppen = new HashMap<String, Knop>();
	private World world;
	private SchermOpbouw schermOpbouw;
	private Sprite opgebouwdScherm;
	private SpriteBatch besturingsVeldSpriteBatch = new SpriteBatch();
	private TijdverloopActie klok;
	private MeldingTextbox meldingTextbox;
	private EventBus eventBus;
	private GameController gameController;
	private Scorebox scorebox;
	private StartKnop startKnop;
	private Vector2 rondeStartButton;

	/**
	 * 
	 * @param world
	 * @param gameController
	 * @param eventbus
	 *            Hieraan kunnen velden op het BesturingsVeld geregistreerd
	 *            worden als listener.
	 */
	public BesturingsVeld(World world, GameController gameController, EventBus eventBus) {
		this.world = world;
		this.eventBus = eventBus;
		this.gameController = gameController;
		initKnoppen();
		initSchermOpbouw("data/besturingsVeld");
		klok = new TijdverloopActie(this, eventBus);
		// scorebox voor meldingTextbox !

		scorebox = new Scorebox(eventBus);
		meldingTextbox = new MeldingTextbox(eventBus);
		//startKnop = StartKnop.getInstance(gameController, world, this, eventBus);
		
		
	}

	private void initKnoppen() {
		knoppen.put(KnopType.LADDER.name(), new LadderKnop(KnopType.LADDER));
		knoppen.put(KnopType.BOOT.name(), new BootKnop(KnopType.BOOT));
		knoppen.put(KnopType.PAUZEKNOP.name(), new PauzeKnop(KnopType.PAUZEKNOP));
		knoppen.put(KnopType.SCHEPJE.name(), new SchepjeKnop(KnopType.SCHEPJE));
		this.startKnop = new StartKnop(KnopType.STARTKNOP, gameController, eventBus);
		knoppen.put(KnopType.STARTKNOP.name(), this.startKnop);
		knoppen.put(KnopType.STOPKNOP.name(), new StopKnop(KnopType.STOPKNOP));
	}

	/**
	 * @param scherm
	 * @return
	 */
	public Sprite initSchermOpbouw(String scherm) {
		schermOpbouw = new SchermOpbouw(scherm, world, this);
		opgebouwdScherm = schermOpbouw.loadGeometry();
		// zat bij Jan Edwin er nog in.. 
		//rondeStartButton = schermOpbouw.getrondeStartButton();
		return opgebouwdScherm;
	}

	
	/**
	 * @param path
	 * @param box
	 */
	public void addKnop(Element path, Body box) {
		String type = path.attr("KnopType");
		if (type != null && type != "") {
			// Koppel userdate en knop.
			Knop knop = knoppen.get(type);
			if (knop != null) {
				box.setUserData(knop);
			} 
		}
	}
	
	public Vector2 getrondeStartButton() {
		return schermOpbouw.getrondeStartButton();
	}

	/**
	 * @param camera
	 */
	public void renderBesturingsVeld(OrthographicCamera camera) {
		// step the world
		if (startKnop.isSpelGestart()) {
			world.step(Gdx.graphics.getDeltaTime(), 4, 4);
		}
		besturingsVeldSpriteBatch.setProjectionMatrix(camera.combined);
		// tekenen van achtergrond
		besturingsVeldSpriteBatch.begin();
		opgebouwdScherm.draw(besturingsVeldSpriteBatch);
		besturingsVeldSpriteBatch.end();
		// meldingen bijwerken
		meldingTextbox.update();
		// scores bijwerken
		scorebox.update();
		// timer bijwerken
		klok.update();
		// startknop plaatje bijwerken
		startKnop.update();
	}

	public Scorebox getScorebox() {
		return scorebox;
	}

	public StartKnop getStartKnop() {
		return startKnop;
	}

}