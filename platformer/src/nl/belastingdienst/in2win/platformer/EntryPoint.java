package nl.belastingdienst.in2win.platformer;

import nl.belastingdienst.in2win.actie.MuziekSpeler;
import nl.belastingdienst.in2win.eventbus.EventBus;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.World;
 
public class EntryPoint  implements ApplicationListener {
 
	private OrthographicCamera camera;
	private GameController gamecontroller;
	private World world;
	 
	EventBus eventBus = new EventBus();
	
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#create()
	 */
	@Override
	public void create() {
		
		gamecontroller = new GameController();
		this.world=gamecontroller.getWorld();
		
		camera = new OrthographicCamera(28, 20);
		new InputAfhandeling(this, this.world);
		System.out.println("camera in EntryPoint:"+ camera);
		
		new MuziekSpeler(eventBus);

		
		gamecontroller.maakSpeelveld(eventBus);
		gamecontroller.maakLadder(eventBus);
		gamecontroller.maakSchepje(eventBus);
		gamecontroller.maakBoot(eventBus);
		gamecontroller.maakBesturingsveld(eventBus);
		gamecontroller.maakUitlegVeld(eventBus);

	}
	
	@Override
	public void resume() {
		//	Geen implementatie nodig.
	}
 
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#render()
	 */
	@Override
	public void render() {
		// scherm schoonmaken en kleur opzetten.
		Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		// Update the camera
		camera.position.set(camera.viewportWidth/2, camera.viewportHeight/2, 0);
		camera.zoom = 1;
		camera.update();
		camera.apply(Gdx.gl10);
		// refresh beeldscherm.
		gamecontroller.render(camera, eventBus);
		
	//	door onderstaande uit te schakelen worden de lijntjes om objecten uitgezet	
	//	renderer.render(world, cam.combined);
	}
 
	@Override
	public void resize(int width, int height) {
		//	Geen implementatie nodig.
	}
 
	@Override
	public void pause() {
		//	Geen implementatie nodig.
	}
 
	@Override
	public void dispose() {
		//	Geen implementatie nodig.
	}

	public OrthographicCamera getCamera() {
		return camera;
	}
 }
