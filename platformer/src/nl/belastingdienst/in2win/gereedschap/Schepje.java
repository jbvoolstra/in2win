package nl.belastingdienst.in2win.gereedschap;

import java.util.concurrent.TimeUnit;

import nl.belastingdienst.in2win.actie.Scorebox;
import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gameSturing.Knop;
import nl.belastingdienst.in2win.gameSturing.KnopType;
import nl.belastingdienst.in2win.locaties.Locatie;
import nl.belastingdienst.in2win.locaties.LocatieType;
import nl.belastingdienst.in2win.locaties.OnderwereldLocatie;
import nl.belastingdienst.in2win.platformer.GameController;
import nl.belastingdienst.in2win.platformer.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Schepje implements EventListener {

	private boolean olgaWilSchepje = false; 
	private boolean olgaWilGereedschap = false;
	private Scorebox scorebox = null;
	private GameController gameController = null;
	
	private int prijs;
	private Player player;
	private boolean schepje1 = false;
	private boolean schepje2 = false;
	private boolean schepje3 = false;
	private Texture img_schepje = new Texture(Gdx.files.internal("data/schepje.png"));
	private SpriteBatch schepjeBatch = new SpriteBatch();
	private boolean toonSchepje;

	public Schepje(EventBus eventBus, GameController gameController) {

		this.prijs = GereedschapType.SCHEP.getPrijs();
		this.player = gameController.getSpeelVeld().getPlayer();

		eventBus.register(EventType.CONTACT, this);
		eventBus.register(EventType.EINDECONTACT, this);
		eventBus.register(EventType.TOETSINGEDRUKT, this);
	}

	@Override
	public void perform(Event event) {

		if (event.getTypeEvent().equals(EventType.EINDECONTACT)) {
				this.olgaWilSchepje = false;
				this.olgaWilGereedschap = false;
			}

		if (event.getTypeEvent().equals(EventType.TOETSINGEDRUKT)) {
			if (event.getVeroorzaker() instanceof Knop) {
				Knop knop = (Knop) event.getVeroorzaker();
				if (knop.getType().equals(KnopType.SCHEPJE)) {
					olgaWilSchepje = true;
					olgaWilGereedschapNamelijkSchepje();
				} else {
					olgaWilSchepje = false;
				}
			} else {
				olgaWilSchepje = false;
			}
		} 

		if (event.getTypeEvent().equals(EventType.CONTACT)) {

			olgaWilGereedschap = false;
			
			if (this.scorebox == null) {
				try {this.scorebox = this.gameController.getBesturingsVeld().getScorebox();} catch (NullPointerException np) {
				}
			}

			if (event.getVeroorzaker() instanceof Locatie) {
				Locatie locatie = (Locatie) event.getVeroorzaker();
				
				if (locatie.getType().equals(LocatieType.TOOLSHED)) {

					olgaWilGereedschap = true;
					olgaWilGereedschapNamelijkSchepje();

				}

				// als Olga een schepje heeft gekocht kunnen we die vast bij de
				// Onderwereld neerleggen..
				if (locatie.getType() == LocatieType.ONDERWERELD) {

					if (player.getGereedschapType() == GereedschapType.SCHEP) {
						@SuppressWarnings("rawtypes")
						OnderwereldLocatie onderwereldLocatie = (OnderwereldLocatie) locatie;
						onderwereldLocatie.addSchepje();
						player.resetGereedschapType();

						// 1e, 2e of 3e schepje
						if (!schepje1) {
							schepje1 = true;
						} else if (!schepje2) {
							schepje2 = true;
						} else if (!schepje3) {
							schepje3 = true;
						}
						// Toon de laatste schepje voor x seconden
						if (onderwereldLocatie.voldoendeGereedschapAanwezig) {
							toonDerdeSchepje();
						}
					}
				}

			}
		}
	}

	private void olgaWilGereedschapNamelijkSchepje() {

		if (this.olgaWilSchepje & this.olgaWilGereedschap) {

			// eigenlijk moet Olga
			if (this.scorebox.getScore() >= this.prijs) {

				// Olga kan één stuk gereedschap tegelijkertijd
				// meenemen.
				if (player.getGereedschapType() == null) {
					player.setGereedschapType(GereedschapType.SCHEP);
					this.olgaWilSchepje = false;
					try {this.scorebox.setScore(this.scorebox.getScore() - this.prijs);} catch (NullPointerException np) {
					}
				}

			}
		}
	}

	/**
	 * de render methode van schepje. teken 1, 2 of 3 schepjes bij de
	 * onderwereldlocatie.
	 */
	public void updateSchepjeBijDeOnderwereld() {

		// TODO misschien kan de SpriteBatch vanuit speelveld meegegeven worden?
		// uitzoeken.
		schepjeBatch.begin();

		if (schepje1) {
			schepjeBatch.draw(img_schepje, 125, 130, 48, 48);
		}
		if (schepje2) {
			schepjeBatch.draw(img_schepje, 150, 120, 48, 48);
		}
		if (schepje3) {
			// tonen 3e schepje is afhankelijk van thread (methode
			// toonDerdeSchepje).
			// Schepje wordt 5 seconden getoond (ge-drawd).
			if (toonSchepje) {
				schepjeBatch.draw(img_schepje, 175, 110, 48, 48);
			}
		}
		schepjeBatch.end();
	}

	public void updateSchepjeBijOlga() {
		if (player.isMagSchepjeMeenemen()) {
			schepjeBatch.begin();
			if (player.isMagSchepjeMeenemen()) {

				float x = player.getPosition().x * 32;
				float y = player.getPosition().y * 34;

				schepjeBatch.draw(img_schepje, x, y, 48, 48);
			}
			schepjeBatch.end();
		}
	}

	private void toonDerdeSchepje() {
		toonSchepje = true;
		// declaratie
		Runnable schepje = new Runnable() {

			@Override
			public void run() {
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// na x seconden niet meer updaten(=draw) en aantal schepjes
				// initieren.
				toonSchepje = false;
				schepje1 = false;
				schepje2 = false;
				schepje3 = false;
			}
		};
		// gebruik
		Thread gebruiktDeTimeout = new Thread(schepje);
		gebruiktDeTimeout.start();
	}

}