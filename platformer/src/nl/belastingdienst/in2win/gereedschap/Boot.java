package nl.belastingdienst.in2win.gereedschap;

import java.util.concurrent.TimeUnit;

import nl.belastingdienst.in2win.actie.Scorebox;
import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gameSturing.Knop;
import nl.belastingdienst.in2win.gameSturing.KnopType;
import nl.belastingdienst.in2win.locaties.Locatie;
import nl.belastingdienst.in2win.locaties.LocatieType;
import nl.belastingdienst.in2win.locaties.BelastingParadijsLocatie;
import nl.belastingdienst.in2win.platformer.GameController;
import nl.belastingdienst.in2win.platformer.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Boot implements EventListener {

	private boolean olgaWilBoot = false; 
	private boolean olgaWilGereedschap = false;
	private Scorebox scorebox = null;
	private GameController gameController = null;
	
	private int prijs;
	private Player player;
	private boolean boot1 = false;
	private boolean boot2 = false;
	private boolean boot3 = false;
	private Texture img_boot = new Texture(Gdx.files.internal("data/boot.png"));
	private SpriteBatch bootBatch = new SpriteBatch();
	private boolean toonBoot;

	public Boot(EventBus eventBus, GameController gameController) {

		this.prijs = GereedschapType.BOOT.getPrijs();
		this.player = gameController.getSpeelVeld().getPlayer();

		eventBus.register(EventType.CONTACT, this);
		eventBus.register(EventType.EINDECONTACT, this);
		eventBus.register(EventType.TOETSINGEDRUKT, this);
	}

	@Override
	public void perform(Event event) {

		if (event.getTypeEvent().equals(EventType.EINDECONTACT)) {
				this.olgaWilBoot = false;
				this.olgaWilGereedschap = false;
			}

		if (event.getTypeEvent().equals(EventType.TOETSINGEDRUKT)) {
			if (event.getVeroorzaker() instanceof Knop) {
				Knop knop = (Knop) event.getVeroorzaker();
				if (knop.getType().equals(KnopType.BOOT)) {
					olgaWilBoot = true;
					olgaWilGereedschapNamelijkBoot();
				} else {
					olgaWilBoot = false;
				}
			} else {
				olgaWilBoot = false;
			}
		} 

		if (event.getTypeEvent().equals(EventType.CONTACT)) {

			olgaWilGereedschap = false;
			
			if (this.scorebox == null) {
				try {this.scorebox = this.gameController.getBesturingsVeld().getScorebox();} catch (NullPointerException np) {
				}
			}

			if (event.getVeroorzaker() instanceof Locatie) {
				Locatie locatie = (Locatie) event.getVeroorzaker();
				
				if (locatie.getType().equals(LocatieType.TOOLSHED)) {

					olgaWilGereedschap = true;
					olgaWilGereedschapNamelijkBoot();

				}

				// als Olga een boot heeft gekocht kunnen we die vast bij de
				// BelastingParadijs neerleggen..
				if (locatie.getType() == LocatieType.BELASTINGPARADIJS) {

					if (player.getGereedschapType() == GereedschapType.BOOT) {
						@SuppressWarnings("rawtypes")
						BelastingParadijsLocatie belastingParadijsLocatie = (BelastingParadijsLocatie) locatie;
						belastingParadijsLocatie.addBoot();
						player.resetGereedschapType();

						// 1e, 2e of 3e boot
						if (!boot1) {
							boot1 = true;
						} else if (!boot2) {
							boot2 = true;
						} else if (!boot3) {
							boot3 = true;
						}
						// Toon de laatste boot voor x seconden
						if (belastingParadijsLocatie.voldoendeGereedschapAanwezig) {
							toonDerdeBoot();
						}
					}
				}

			}
		}
	}

	private void olgaWilGereedschapNamelijkBoot() {

		if (this.olgaWilBoot & this.olgaWilGereedschap) {

			// eigenlijk moet Olga
			if (this.scorebox.getScore() >= this.prijs) {

				// Olga kan één stuk gereedschap tegelijkertijd
				// meenemen.
				if (player.getGereedschapType() == null) {
					player.setGereedschapType(GereedschapType.BOOT);
					this.olgaWilBoot = false;
					try {this.scorebox.setScore(this.scorebox.getScore() - this.prijs);} catch (NullPointerException np) {
					}
				}

			}
		}
	}

	/**
	 * de render methode van boot. teken 1, 2 of 3 boots bij de
	 * belastingParadijslocatie.
	 */
	public void updateBootBijHetBelastingParadijs() {

		// TODO misschien kan de SpriteBatch vanuit speelveld meegegeven worden?
		// uitzoeken.
		bootBatch.begin();

		if (boot1) {
			bootBatch.draw(img_boot, 625, 730, 48, 48);
		}
		if (boot2) {
			bootBatch.draw(img_boot, 650, 720, 48, 48);
		}
		if (boot3) {
			// tonen 3e boot is afhankelijk van thread (methode
			// toonDerdeBoot).
			// Boot wordt 5 seconden getoond (ge-drawd).
			if (toonBoot) {
				bootBatch.draw(img_boot, 675, 710, 48, 48);
			}
		}
		bootBatch.end();
	}

	public void updateBootBijOlga() {
		if (player.isMagBootMeenemen()) {
			bootBatch.begin();
			if (player.isMagBootMeenemen()) {

				float x = player.getPosition().x * 32;
				float y = player.getPosition().y * 34;

				bootBatch.draw(img_boot, x, y, 48, 48);
			}
			bootBatch.end();
		}
	}

	private void toonDerdeBoot() {
		toonBoot = true;
		// declaratie
		Runnable boot = new Runnable() {

			@Override
			public void run() {
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// na x seconden niet meer updaten(=draw) en aantal boots
				// initieren.
				toonBoot = false;
				boot1 = false;
				boot2 = false;
				boot3 = false;
			}
		};
		// gebruik
		Thread gebruiktDeTimeout = new Thread(boot);
		gebruiktDeTimeout.start();
	}

}
