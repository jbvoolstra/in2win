package nl.belastingdienst.in2win.gereedschap;

import java.util.concurrent.TimeUnit;

import nl.belastingdienst.in2win.actie.Scorebox;
import nl.belastingdienst.in2win.eventbus.Event;
import nl.belastingdienst.in2win.eventbus.EventBus;
import nl.belastingdienst.in2win.eventbus.EventListener;
import nl.belastingdienst.in2win.eventbus.EventType;
import nl.belastingdienst.in2win.gameSturing.Knop;
import nl.belastingdienst.in2win.gameSturing.KnopType;
import nl.belastingdienst.in2win.gameSturing.LadderKnop;
import nl.belastingdienst.in2win.locaties.Locatie;
import nl.belastingdienst.in2win.locaties.LocatieType;
import nl.belastingdienst.in2win.locaties.ZwitsersebankLocatie;
import nl.belastingdienst.in2win.platformer.GameController;
import nl.belastingdienst.in2win.platformer.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Ladder implements EventListener {

	private boolean olgaWilLadder = false; 
	private boolean olgaWilGereedschap = false;
	private Scorebox scorebox = null;
	private GameController gameController = null;
	
	private int prijs;
	private Player player;
	private boolean ladder1 = false;
	private boolean ladder2 = false;
	private boolean ladder3 = false;
	private Texture img_ladder = new Texture(Gdx.files.internal("data/ladder.png"));
	private SpriteBatch ladderBatch = new SpriteBatch();
	private boolean toonLadder;

	public Ladder(EventBus eventBus, GameController gameController) {

		this.prijs = GereedschapType.LADDER.getPrijs();
		this.player = gameController.getSpeelVeld().getPlayer();

		eventBus.register(EventType.CONTACT, this);
		eventBus.register(EventType.EINDECONTACT, this);
		eventBus.register(EventType.TOETSINGEDRUKT, this);
	}

	@Override
	public void perform(Event event) {

		if (event.getTypeEvent().equals(EventType.EINDECONTACT)) {
				this.olgaWilLadder = false;
				this.olgaWilGereedschap = false;
			}

		if (event.getTypeEvent().equals(EventType.TOETSINGEDRUKT)) {
			if (event.getVeroorzaker() instanceof Knop) {
				Knop knop = (Knop) event.getVeroorzaker();
				if (knop.getType().equals(KnopType.LADDER)) {
					olgaWilLadder = true;
					olgaWilGereedschapNamelijkLadder();
				} else {
					olgaWilLadder = false;
				}
			} else {
				olgaWilLadder = false;
			}
		} 

		if (event.getTypeEvent().equals(EventType.CONTACT)) {

			olgaWilGereedschap = false;
			
			if (this.scorebox == null) {
				try {this.scorebox = this.gameController.getBesturingsVeld().getScorebox();} catch (NullPointerException np) {
				}
			}

			if (event.getVeroorzaker() instanceof Locatie) {
				Locatie locatie = (Locatie) event.getVeroorzaker();
				
				if (locatie.getType().equals(LocatieType.TOOLSHED)) {

					olgaWilGereedschap = true;
					olgaWilGereedschapNamelijkLadder();

				}

				// als Olga een ladder heeft gekocht kunnen we die vast bij de
				// Zwitsersebank neerleggen..
				if (locatie.getType() == LocatieType.ZWITSERSEBANK) {

					if (player.getGereedschapType() == GereedschapType.LADDER) {
						@SuppressWarnings("rawtypes")
						ZwitsersebankLocatie zwitsersebankLocatie = (ZwitsersebankLocatie) locatie;
						zwitsersebankLocatie.addLadder();
						player.resetGereedschapType();

						// 1e, 2e of 3e ladder
						if (!ladder1) {
							ladder1 = true;
						} else if (!ladder2) {
							ladder2 = true;
						} else if (!ladder3) {
							ladder3 = true;
						}
						// Toon de laatste ladder voor x seconden
						if (zwitsersebankLocatie.voldoendeGereedschapAanwezig) {
							toonDerdeLadder();
						}
					}
				}

			}
		}
	}

	private void olgaWilGereedschapNamelijkLadder() {

		if (this.olgaWilLadder & this.olgaWilGereedschap) {

			// eigenlijk moet Olga
			if (this.scorebox.getScore() >= this.prijs) {

				// Olga kan één stuk gereedschap tegelijkertijd
				// meenemen.
				if (player.getGereedschapType() == null) {
					player.setGereedschapType(GereedschapType.LADDER);
					this.olgaWilLadder = false;
					try {this.scorebox.setScore(this.scorebox.getScore() - this.prijs);} catch (NullPointerException np) {
					}
				}

			}
		}
	}

	/**
	 * de render methode van ladder. teken 1, 2 of 3 ladders bij de
	 * zwitsersebanklocatie.
	 */
	public void updateLadderBijDeZwitserseBank() {

		// TODO misschien kan de SpriteBatch vanuit speelveld meegegeven worden?
		// uitzoeken.
		ladderBatch.begin();

		if (ladder1) {
			ladderBatch.draw(img_ladder, 50, 730, 48, 48);
		}
		if (ladder2) {
			ladderBatch.draw(img_ladder, 75, 720, 48, 48);
		}
		if (ladder3) {
			// tonen 3e ladder is afhankelijk van thread (methode
			// toonDerdeLadder).
			// Ladder wordt 5 seconden getoond (ge-drawd).
			if (toonLadder) {
				ladderBatch.draw(img_ladder, 100, 710, 48, 48);
			}
		}
		ladderBatch.end();
	}

	public void updateLadderBijOlga() {
		if (player.isMagLadderMeenemen()) {
			ladderBatch.begin();
			if (player.isMagLadderMeenemen()) {

				float x = player.getPosition().x * 32;
				float y = player.getPosition().y * 34;

				ladderBatch.draw(img_ladder, x, y, 48, 48);
			}
			ladderBatch.end();
		}
	}

	private void toonDerdeLadder() {
		toonLadder = true;
		// declaratie
		Runnable ladder = new Runnable() {

			@Override
			public void run() {
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// na x seconden niet meer updaten(=draw) en aantal ladders
				// initieren.
				toonLadder = false;
				ladder1 = false;
				ladder2 = false;
				ladder3 = false;
			}
		};
		// gebruik
		Thread gebruiktDeTimeout = new Thread(ladder);
		gebruiktDeTimeout.start();
	}

}
