package nl.belastingdienst.in2win.gereedschap;

public enum GereedschapType {
	SCHEP(150,2), LADDER(150,3), BOOT(100,2);

	private int prijs;
	private int aantal;

	private GereedschapType(int prijs, int aantal) {
		this.prijs = prijs;
		this.aantal = aantal;
	}

	public int getPrijs() {
		return prijs;
	}
	
	public int getAantal() {
		return aantal;
	}
}
